use strict;

use vars qw($VERSION %IRSSI);

use Irssi;

$VERSION = '1.00';
%IRSSI = (
  authors     => 'ptrv',
  contact     => 'mail@petervasil.net',
  name        => 'NickServ Gnome Keyring Identification',
  description => 'This script allows you to identify to NickServ using the' .
                 'stored password from the Gnome Keyring.',
  license     => 'GNU GPL v3'
);

sub cmd_identify {
  my ($data, $server, $witem) = @_;

  if (!$server || !$server->{connected}) {
    Irssi::print("Not connected to server.", MSGLEVEL_CLIENTERROR);
    return;
  }

  if ($data) {
    chomp(my $password = `keyring.py get "$data" 2>&1`);
    if ($? != 0) {
      Irssi::print("Unable to retrieve password for account '$data':\n$password", MSGLEVEL_CLIENTERROR);
      return;
    }
    $server->command("^MSG NickServ identify $password");
    Irssi::print("Sent identification for $data to NickServ.", MSGLEVEL_CLIENTNOTICE);
  } else {
    Irssi::print("Account name required.", MSGLEVEL_CLIENTERROR);
  }
}

Irssi::command_bind('identify', \&cmd_identify);
