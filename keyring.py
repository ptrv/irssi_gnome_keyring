#! /usr/bin/env python2

import gnomekeyring as gkey

def set_credentials(repo, user, pw):
    KEYRING_NAME = "irssi"
    attrs = { "repo": repo, "user": user }
    keyring = gkey.get_default_keyring_sync()
    gkey.item_create_sync(keyring, gkey.ITEM_NETWORK_PASSWORD,
        KEYRING_NAME, attrs, pw, True)

def get_credentials(repo):
    keyring = gkey.get_default_keyring_sync()
    attrs = {"repo": repo}
    items = gkey.find_items_sync(gkey.ITEM_NETWORK_PASSWORD, attrs)
    return (items[0].attributes["user"], items[0].secret)

def get_username(repo):
    return get_credentials(repo)[0]
def get_password(repo):
    return get_credentials(repo)[1]

if __name__ == "__main__":
    import sys
    import os
    import getpass
    import argparse

    def do_set(args):
        password = getpass.getpass("Enter password for user '%s': " % args.user)
        password_confirmation = getpass.getpass("Confirm password: ")
        if password != password_confirmation:
            print "Error: password confirmation does not match"
            sys.exit(1)
        set_credentials(args.repo, args.user, password)

    def do_get(args):
        try:
            print(get_password(args.repo))
            sys.exit(0)
        except gkey.NoMatchError:
            sys.exit(1)

    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers()
    parser_get = subparsers.add_parser('get', help='get password')
    parser_get.add_argument('repo', help='name of the repository')
    parser_get.set_defaults(func=do_get)
    parser_set = subparsers.add_parser('set', help='set password')
    parser_set.add_argument('repo', help='name of the repository')
    parser_set.add_argument('user', help='username')
    parser_set.set_defaults(func=do_set)

    args = parser.parse_args()
    args.func(args)
